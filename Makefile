PACKAGE=cobra
PYTHON=$(shell which python3.6)
SUDO=$(shell which sudo)
PIP=$(shell which pip)

default:
	./cobra/compiler.py main.cbr

install: submodule_init
	$(SUDO) apt-get update

	$(PIP) install --upgrade .

	$(SUDO) apt-get install -y libgmp-dev

install_git:
	$(SUDO) apt-get install -y git

submodule_init: install_git
	git submodule update --init --recursive

test:
	$(PYTHON) `which nosetests` --nocapture --with-coverage --cover-package=$(PACKAGE) --processes=8 --cover-erase
	coverage report -m --skip-covered --omit=env

	@rm -f $(PACKAGE)/.coverage*