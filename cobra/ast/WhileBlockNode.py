from cobra.ast.BlockNode import *

class WhileBlockNode(BlockNode):
    def __init__(self, expression):
        super().__init__(expression)

        self._booleanExpression = expression.lower().strip() \
            .replace('while ', '', 1)

    def __str__(self):
        return 'while ({}) {{'.format(self._booleanExpression)
