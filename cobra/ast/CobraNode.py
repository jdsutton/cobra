class CobraNode:
    def __init__(self, expression=''):
        self._expression = expression
        self._children = []
        self._varTypesByVarName = dict()
        self._indentationLevel = 0

        while self._indentationLevel < len(expression) \
        and expression[self._indentationLevel] is ' ':
            self._indentationLevel += 1

    def getIndentationLevel(self):
        return self._indentationLevel

    def canAcceptChildren(self):
        return True

    @property
    def children(self):
        return self._children
    
    def append(self, node):
        # if len(self._children) > 0:
        #     child = self._children[-1]

        #     try:
        #         child.append(node)
        #     except CobraNode.OperationNotSupportedError:
        #         self._children.append(node)
        # else:
        self._children.append(node)

    def __str__(self): return ''
