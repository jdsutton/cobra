from cobra.ast.CobraNode import *
from cobra.UnescapedCharacterIndexIterator import *

class ExpressionNode(CobraNode):
    def __init__(self, expression):
        super().__init__(expression)

        self._expression = expression.strip()

    def __str__(self):
        return self._expression + ';'

    def canAcceptChildren(self):
        return False
        