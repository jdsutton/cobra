from cobra.ast.CobraNode import *
from cobra.UnescapedCharacterIndexIterator import *

class StdoutNode(CobraNode):
    def __init__(self, expression):
        super().__init__(expression)

        self._value = expression.replace('Print ', '').strip()
    
        for index in UnescapedCharacterIndexIterator(self._value, '\''):
                self._value = self._value[:index] + '"' + self._value[index + 1:]

    def __str__(self):
        return 'cout << {} << \'\\n\';'.format(self._value)

    def canAcceptChildren(self):
        return False
        