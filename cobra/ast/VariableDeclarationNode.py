from cobra.ast.CobraNode import *

class VariableDeclarationNode(CobraNode):
    _NUMBER_CLASS = 'double'
    
    def __init__(self, expression):
        super().__init__(expression)

        declarations = expression[4:].rstrip().split(',')  
        
        self._variables = []
        self._values = []

        for declaration in declarations:
            variable, value = [x.strip().replace('Let ', '') for x in declaration.split('=')]    

            self._variables.append(variable)
            self._values.append(value)
    
    def __str__(self):
        declarations = []

        for variable, value in zip(self._variables, self._values):
            variableType = self._getVariableTypeFromValue(value)
            declaration = '{cls} {variable} = {value};'.format(
                cls=variableType,
                value=value,
                variable=variable,
            )
            declarations.append(declaration)

        return '\n'.join(declarations)

    def _getVariableTypeFromValue(self, value):
        if value.startswith('\'') or value.startswith('"'):
            return 'CobraString'
        elif value in ['true', 'false']:
            return 'CobraBoolean'

        # TODO: Expression.

        return self.__class__._NUMBER_CLASS

    def canAcceptChildren(self):
        return False