from cobra.ast.CloseBlockNode import *
from cobra.ast.BlockNode import *

class OpenIfBlockNode(BlockNode):
    def __init__(self, expression):
        super().__init__(expression)

        self._booleanExpression = expression.strip()[3:].strip()
        self._canAcceptChildren = True

    def canAcceptChildren(self):
        return self._canAcceptChildren

    def addChild(self, node):
        super().addChild(node)

        if isinstance(node, CloseBlockNode):
            self._canAcceptChildren = False

    def __str__(self):
        result =  'if ({}) {{'.format(self._booleanExpression)

        # for child in self._children:
        #     result = result + str(child)
        
        return result