from cobra.ast.BlockNode import *
from cobra.UnescapedStringReplacer import UnescapedStringReplacer

class ElseBlockNode(BlockNode):
    def __init__(self, expression):
        super().__init__(expression)

        expression = expression.lower().strip()

        if expression.startswith('else if '):
            self._else = 'else if'
            expression = expression[7:]
        else:
            self._else = 'else'
            expression = expression[4:]

        self._booleanExpression = UnescapedStringReplacer.replace(
            expression.strip(),
            ' is ',
            '==',
        )

    def __str__(self):
        if (self._else == 'else'):
            return 'else {'
        
        return '{} ({}) {{'.format(self._else, self._booleanExpression)
