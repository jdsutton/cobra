from cobra.ast.CobraNode import *

class CloseBlockNode(CobraNode):
    def __init__(self, expression):
        super().__init__(expression)

    def canAcceptChildren(self):
        return False

    def __str__(self):
        return '}'