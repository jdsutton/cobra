from cobra.ast.CobraNode import *

class AssignmentNode(CobraNode):
    def __init__(self, expression):
        super().__init__(expression)

        stripped = expression.replace('Set ', '').strip()
        self._var, self._value = stripped.split(' to ')
    
    def __str__(self):
        return '{} = {};'.format(self._var, self._value)
        