class CobraNodeIterator:
    def __init__(self, root):
        self._stack = [root]

    def __iter__(self): return self

    def __next__(self):
        if len(self._stack) == 0:
            raise StopIteration()

        node = self._stack.pop()

        self._stack += reversed(node.children)

        return node
