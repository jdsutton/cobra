from cobra.ast.AssignmentNode import *
from cobra.ast.BlockNode import *
from cobra.ast.CloseBlockNode import *
from cobra.ast.CobraNode import *
from cobra.ast.CobraNodeIterator import *
from cobra.ast.ElseBlockNode import *
from cobra.ast.ExpressionNode import *
from cobra.ast.OpenIfBlockNode import *
from cobra.ast.StdoutNode import *
from cobra.ast.WhileBlockNode import *
from cobra.ast.VariableDeclarationNode import *
from cobra.StatementIterator import *
import subprocess

class CobraProgram:
    '''
    Handles programs written in Cobra.
    '''

    _COMPILE_BASH = 'g++ -o {outFile} {args} {inFile}'
    _C_HEADER = '''
        #include <iostream>
        using namespace std;

        #include <gmpxx.h>

        int main(){
    '''
    _C_FOOTER = '\n}'
    _TEMP_FILE_NAME = '_cobra_out.cpp'

    class UnhandledStatementError(RuntimeError):
        pass

    def __init__(self, filePath, *, executablePath='out.exe', cArgs=''):
        self._filePath = filePath
        self._executablePath = executablePath
        self._cArgs = cArgs
        self._root = CobraNode()
        self._lastNode = self._root
        self._openIfNode = None
        self._cCode = None

        self._parse()

    def _parse(self):
        prevIndentation = 0

        with open(self._filePath, 'r') as f:
            lines = f.readlines()

        for statement in StatementIterator(lines):
            stripped = statement.lstrip()

            if self._hasOpenBlock():
                closeNode = CloseBlockNode(statement)

                if closeNode.getIndentationLevel() <= self._openIfNode.getIndentationLevel():
                    self._appendNode(closeNode)

            # TODO: Use some trie here.
            if stripped.startswith('Let ') or stripped.startswith('let '):
                node = VariableDeclarationNode(statement)
            elif stripped.startswith('Set ') or stripped.startswith('set '):
                node = AssignmentNode(statement)
            elif stripped.startswith('Print ') or stripped.startswith('print '):
                node = StdoutNode(statement)
            elif stripped.startswith('If ') or stripped.startswith('if ') :
                node = OpenIfBlockNode(statement)
            elif stripped.startswith('Else ') or stripped.startswith('else ') or stripped == 'else':
                node = ElseBlockNode(statement)
            elif stripped.startswith('While ') or stripped.startswith('while '):
                node = WhileBlockNode(statement)
            else:
                node = ExpressionNode(statement)

            self._appendNode(node)

            prevIndentation = node.getIndentationLevel()

        if self._hasOpenBlock():
            self._appendNode(CloseBlockNode(''))

    def _hasOpenBlock(self):
        return self._openIfNode is not None

    def _appendNode(self, node):
        '''
        Append the given node to the last node in the tree which is accepting children.
        '''

        self._root.append(node)

        if isinstance(node, CloseBlockNode):
            self._openIfNode = None
        elif isinstance(node, BlockNode):
            self._openIfNode = node

    def _transpileCobra(self):
        lines = [
            str(node)
            for node in CobraNodeIterator(self._root)
        ]

        self._cCode = self.__class__._C_HEADER \
            + '\n'.join(lines) \
            + self.__class__._C_FOOTER

    def _compileC(self):
        with open(CobraProgram._TEMP_FILE_NAME, 'w') as f:
            f.write(self._cCode)

        command = CobraProgram._COMPILE_BASH.format(
            inFile=CobraProgram._TEMP_FILE_NAME,
            outFile=self._executablePath,
            args=self._cArgs,
        )

        subprocess.run(command, shell=True, check=True)

    def compile(self):
        if self._cCode is not None: return

        self._transpileCobra()

        self._compileC()

        return self

    def execute(self):
        self.compile()
