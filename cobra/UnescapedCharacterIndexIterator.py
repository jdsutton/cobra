class UnescapedCharacterIndexIterator:

    _ESCAPE_CHARS = set([
        '\'',
        '"',
    ])

    def __init__(self, string, char):
        self._string = string
        self._char = char
        self._openEscapeChar = None
        self._i = 0

    def __iter__(self): return self

    def _matchAtIndex(self, index):
        for i in range(len(self._char)):
            stringIndex = index + i

            if stringIndex >= len(self._string):
                return False

            if self._string[stringIndex] != self._char[i]:
                return False

        return True

    def __next__(self):

        while self._i < len(self._string):
            
            char = self._string[self._i]

            if char in self.__class__._ESCAPE_CHARS:
                if self._openEscapeChar == char:
                    # Close.
                    self._openEscapeChar = None
                else:
                    # Open.
                    self._openEscapeChar = char

                self._i += 1

                if self._matchAtIndex(self._i - 1) and \
                (self._openEscapeChar is None or self._openEscapeChar is char):
                    return self._i - 1

                continue

            self._i += 1

            if self._openEscapeChar is None and self._matchAtIndex(self._i - 1):
                return self._i - 1
            else:
                # Inside escape sequence or wrong char.
                pass

        raise StopIteration()
