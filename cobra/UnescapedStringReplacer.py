from cobra.UnescapedCharacterIndexIterator import *

class UnescapedStringReplacer:

    @classmethod
    def replace(cls, string, toReplace, replaceWith):
        totalDiff = 0
        diff = len(replaceWith) - len(toReplace)

        for index in UnescapedCharacterIndexIterator(string, toReplace):
            i = index + totalDiff

            string = string[:i] + replaceWith + string[i + len(toReplace):]

            totalDiff += diff

        return string 