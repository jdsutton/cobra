from cobra.UnescapedCharacterIndexIterator import *

class StatementIterator:
    '''
    Iterates over Cobra statements.
    '''

    def __init__(self, lines):
        self._queue = []
        self._lines = lines
        self._i = 0

    def _splitOnIndices(self, s, indices):
        end = len(s)

        if len(indices) == 0 or indices[-1] != end:
            indices = indices + [end]
        if len(indices) < 2 or indices[0] != -1:
            indices = [-1] + indices

        return list(filter(lambda x: x, [
            s[indices[i] + 1:indices[i + 1]]
            for i in range(len(indices) - 1)
        ]))

    def __iter__(self): return self

    def __next__(self):
        while len(self._queue) > 0:
            result = self._queue.pop(0)

            if result:
                return result
        
        while self._i < len(self._lines):
            nextLine = self._lines[self._i].rstrip()
            self._i += 1

            splitIndices = list(UnescapedCharacterIndexIterator(nextLine, '.'))

            split = self._splitOnIndices(nextLine, splitIndices)
            split = filter(lambda s: not s.lstrip().startswith('#'), split)
            split = list(split)

            while len(split) > 0:
                statement = split.pop(0)

                if statement:
                    self._queue += split

                    return statement

        raise StopIteration()