import os
import unittest

class test_UnescapedStringReplacer(unittest.TestCase):
    def test_iterate(self):
        from cobra.UnescapedStringReplacer import UnescapedStringReplacer

        code = ' is " is " is '

        result = UnescapedStringReplacer.replace(code, ' is ', '==')

        self.assertEqual(result, '==" is "==')

if __name__ == '__main__':
    unittest.main()