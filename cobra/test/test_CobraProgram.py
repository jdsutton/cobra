import os
import unittest

class test_CobraProgram(unittest.TestCase):
    def test_compile(self):
        from cobra.CobraProgram import CobraProgram

        tempFile = 'test_CobraProgram.test_compile.cbr'
        code = '''
            # Declaration.
            Let x=0, y=0, z=0.

            # Assignment.
            Set x to 10.
            Set y to x * 2 + 10.

            # Stdout.
            Print "Hello World".
            Print 'Hello World'.
            Print y.

            # Stdin.

            # Logic.
            if x < 100
                x--.
            else if y is 0
                Print 'y is zero.'.
            else
                Print 'y is not zero.'.

            # Loops.
            While x < 100
                x++

            #For i in [1, 2, 3]
            #    Print(i)

            # Template strings.
            # Print 'X is [[x]]!'.
        '''

        with open(tempFile, 'w') as f:
            f.write(code)

        program = CobraProgram(tempFile).compile()

        os.remove(tempFile)

if __name__ == '__main__':
    unittest.main()