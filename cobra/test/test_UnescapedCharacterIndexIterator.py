import os
import unittest

class test_UnescapedCharacterIndexIterator(unittest.TestCase):
    def test_iterate(self):
        from cobra.UnescapedCharacterIndexIterator import UnescapedCharacterIndexIterator

        code = '. "." .'

        result = list(UnescapedCharacterIndexIterator(code, '.'))

        self.assertEqual(len(result), 2)
        self.assertEqual(result[0], 0)
        self.assertEqual(result[1], 6)

if __name__ == '__main__':
    unittest.main()