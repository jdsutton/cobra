import os
import unittest

class test_StatementIterator(unittest.TestCase):
    def test_iterate(self):
        from cobra.StatementIterator import StatementIterator

        lines = '''# Declaration.
Let x=0, y=0, z=0.

# Assignment.
Set x to 10.
Set y to x + 10.

if x is 0
Set y to 5.'''.split('\n')

        result = list(StatementIterator(lines))

        self.assertEqual(result[0], 'Let x=0, y=0, z=0')
        self.assertEqual(result[1], 'Set x to 10')
        self.assertEqual(result[2], 'Set y to x + 10')
        self.assertEqual(result[3], 'if x is 0')
        self.assertEqual(result[4], 'Set y to 5')

if __name__ == '__main__':
    unittest.main()