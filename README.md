# Cobra Programming Language
## Code for Humans

## What is Cobra?
Cobra is a programming language strongly focused on readability. Cobra tries to approximate natural language while maintaining disambiguity.

## Installing Cobra
```
$ git clone --recursive https://gitlab.com/jdsutton/cobra.git
$ cd cobra
$ make install
```

## Getting Started
Check the [Example Programs](example_programs) to learn Cobra syntax. You can write you own Cobra program in a .cbr file and run it with:

`$cobra myProgram.cbr`